import streamlit as st
import requests

API_URL_APPS = "https://ojk-invest-api.vercel.app/api/apps"

def search_apps(name):
    params = {"name": name}
    response = requests.get(API_URL_APPS, params=params)
    data = response.json()
    return data

def main():
    st.title("Lembaga Keuangan ft. OJK🫱🏻‍🫲🏻")
    st.subheader("Cek Aplikasi terdaftar atau Tidak")

    name = st.text_input("Masukkan nama lembaga keuangan")
    if st.button("Cari"):
        if name:
            apps_data = search_apps(name)
            if "apps" in apps_data["data"]:
                apps = apps_data["data"]["apps"]
                if apps:
                    for app in apps:
                        st.markdown(f"**Nama\t: {app.get('name')}**")
                        st.write(f"URL\t\t: {app.get('url')}")
                        st.write(f"Pemilik\t: {app.get('owner')}")
                        st.write("---")
                else:
                    st.warning("Data tidak ditemukan.")
            else:
                st.warning("Data tidak ditemukan.")
        else:
            st.warning("Masukkan nama lembaga keuangan.")

if __name__ == "__main__":
    main()

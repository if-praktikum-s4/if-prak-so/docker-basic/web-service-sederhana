# Web Service Sederhana

Projek web service atau web page kali ini melibatkan penggunaan Docker untuk mengemas dan mendistribusikan aplikasi web ke lingkungan produksi yang dapat diakses oleh publik. Dalam konteks ini, Docker digunakan sebagai alat untuk mengelola dan mengisolasi lingkungan aplikasi web, serta memudahkan proses deployment ke lingkungan publik.

Dengan menggunakan Docker untuk deployment ataupun testing, Kita dapat dengan mudah mengemas dan mendistribusikan aplikasi web sederhana ke lingkungan publik tanpa perlu mengkhawatirkan perbedaan konfigurasi atau dependensi di host sistem operasi yang berbeda. Docker memungkinkan Anda untuk memastikan konsistensi dan portabilitas aplikasi di berbagai lingkungan produksi.


## 1. Pembuatan Docker Image
Langkah pertama, kita bisa melakukan `touch Dockerfile` terlebih dahulu atau langsung `nano Dockerfile` saja (tidak perlu nano, sesuaikan dengan editor yang digunakan)
```bash
# Base image
FROM debian:bullseye-slim
# Install dependencies
RUN apt-get update && apt-get install -y git neovim netcat python3-pip
# Copy web service files
COPY invest-threads.py home/invest-threads/invest-threads.py
COPY requirements.txt home/invest-threads/requirements.txt
# Install Python dependencies
RUN pip3 install --no-cache-dir -r /home/invest-threads/requirements.txt
# Set working directory
WORKDIR /home/invest-threads
# Expose port
EXPOSE 25121
# Run the application
CMD ["streamlit", "run", "--server.port=25121", "invest-threads.py" ]

```

Gunakan `docker build -t namaprojek:1.0 .` untuk membangun Docker Image, seperti:
```bash
docker build -t invest-threads:1.0 .
```

Source Code [here](url)
## 2. Web Page dalam Docker Container
Gunakan bahasa pemrograman sesuai dengan preferensi masing-masing, di sini contoh menggunakan python dengan dependencies seperti tertera dalam Dockerfile. Dan kita juga bisa menyertakan file lain jika diperlukan seperti `txt`, `csv`, `xslx`, atau lainnya. 

![Web](https://gitlab.com/if-praktikum-s4/if-prak-so/docker-basic/web-service-sederhana/-/raw/main/img/web-page.png)

Web page bisa di akses di `http://135.181.26.148:25121/` atau [_click here_](http://135.181.26.148:25121/)


## 3. Docker Compose as Distributed Computing
Setelah membangun `Docker Image` dengan `Dockerfile`, buat `docker-compose.yml`untuk kontainerisasi program. Konfigurasi contoh sebagai berikut:
```bash
version: "3"
services:
  invest-threads:
    image: ridwanafazn/invest-threads:1.0
    ports:
      - "25121:25121"
    networks:
      - niat-yang-suci
    volumes:
      - /home/praktikumc/1217050121/invest-threads:/home/invest-threads

networks:
  niat-yang-suci:
    external: true
```
Setelah `docker-compose.yml` berhasil dibuat maka bisa langsung saja build dengan format `docker build -t namaprojek:1.0 .`
```bash
docker build -t invest-threads:1.0 .
```

Lalu kita bisa mengikuti langkah-langkah berikut:
### - Mengganti tag Docker Image

Gunakan `docker tag namaprojek:1.0 namaakun/namaprojek:1.0` jika memang teman-teman ingin memberikan nama alias terhadap tag yang sudah dibuat sebelumnya. Sebelum melakukan publikasi tag ke docker hub
```bash
docker tag invest-threads:1.0 ridwanafazn/invest-threads:1.0
```

### - Buat akun dockerhub 

![registry](https://gitlab.com/if-praktikum-s4/if-prak-so/docker-basic/web-service-sederhana/-/raw/main/img/docker-hub.png)

Jika teman-teman sudah memiliki akun langsung skip saja ke step berikutnya. Namun, jika belum memiliki akun docker hub maka lakukan registrasi terlebih dahulu atau klik [di sini](https://hub.docker.com/) 
### - Login
Lakukan dengan `docker login` dan kita akan diminta memasukkan username dan password, isi dengan akun yang kita buat sebelumnya.
### - Push image Docker
Terlebih dahulu, cek apakah tag Docker Image kita sudah ada di lokal/ssh yang digunakan atau belum dengan memberi perintah `docker images`

Jika sudah ada, jalankan perintah `docker push namaakun/namaprojek:1.0`

```bash
docker push ridwanafazn/invest-threads:1.0
```
Pastikan tag image Docker tersebut sudah ada di repository Docker kita dengan mengeceknya di website docker hub.

### - Jalankan Konfigurasi
Setelah semuanya sudah berjalan sebagaimana mestinya, dan sudah cukup layak untuk bisa diakses oleh publik maka kita bisa menjalankan dengan memberi perintah `socker-compose up` yang akan membaca dan menjalankan file `docker-compose.yml`. 

Hasilnya, projek yang kita bangun sudah bisa diakses di url dan port yang diberikan setelah menjalankan perintah `up` tadi. Kita juga bisa mengecek container yang sudah berjalan dengan perintah `docker ps`
```bash
CONTAINER ID   IMAGE                               COMMAND                     CREATED        STATUS        PORTS                       NAMES
e647d6ee128b   ridwanafazn/invest-threads:1.0      "streamlit run --ser…"      18 hours ago   Up 18 hours   0.0.0.0:25121->25121/tcp    invest-threads_invest-threads_1
```






## 4. Sistem Operasi, Container, & DevOps
### Project Desc
Invest Threads adalah halaman yang bertujuan agar memaksimalkan profit untuk industri. Dalam hal ini, keamanan dan kenyamanan pengguna dalam menikmati layanan keuangan menjadi yang terpenting untuk perusahaan bisa mendapatkan kepercayaan dari orang banyak. 

kepercayaan yang didapatkan pun sejalan dengan profit yang akan didapatkan oleh perusahaan secara jangka panjang, dan menimbulkan simbiosis mutualisme antara pihak Perusahaan dengan Client.

Aplikasi web yang dibangun menggunakan data dari API yang disediakan oleh developer yang bekerja sama dengan OJK atau Otoritas Jasa Keuangan. 
### Peran Sistem Operasi pada Containerization
Sistem operasi (OS) adalah dasar dari containerisasi. Sistem operasi menyediakan kernel, perpustakaan, dan sumber daya lain yang diperlukan oleh kontainer untuk berjalan. Sistem operasi juga memberikan isolasi antara kontainer, yang membantu mencegah saling ganggu antar kontainer.

Dalam konteks containerisasi, sistem operasi berperan sebagai "host" yang menjalankan dan mengelola kontainer, menyediakan sumber daya yang diperlukan, dan memastikan isolasi yang sesuai antara kontainer.
### Container mempermudah Development
Containerisasi dapat membantu dalam pengembangan aplikasi dengan beberapa cara. 
1. Pertama, containerisasi membuat aplikasi lebih mudah dipindahkan. Container dapat dijalankan di mesin apa pun yang memiliki sistem operasi yang sama dengan mesin yang digunakan untuk membuat kontainer. Hal ini memudahkan dalam mendeploy aplikasi ke lingkungan yang berbeda, seperti pengembangan, staging, dan produksi.

2. Kedua, containerisasi dapat mempercepat proses pengembangan. Kontainer dapat dibuat dan didistribusikan dengan cepat, yang dapat membantu mengurangi waktu yang diperlukan untuk menghadirkan fitur-fitur baru ke produksi.

3. Ketiga, containerisasi dapat meningkatkan keamanan aplikasi. Container diisolasi satu sama lain, yang membantu mencegah pelanggaran keamanan dari memengaruhi aplikasi lain.
### What & How DevOps?

![dev-ops-linkedin](https://gitlab.com/if-praktikum-s4/if-prak-so/docker-basic/web-service-sederhana/-/raw/main/img/dev-ops-linkedin.jpeg)

DevOps adalah kumpulan praktik yang menggabungkan pengembangan perangkat lunak (Dev) dan operasi Teknologi Informasi/IT (Ops). Tujuan dari DevOps adalah untuk memperpendek siklus pengembangan dan meningkatkan kualitas aplikasi. DevOps dapat membantu pengembangan aplikasi yaitu sebagai berikut

1. DevOps dapat membantu menghapus sekat antara Tim Dev dan Tim Ops. Hal ini memungkinkan komunikasi dan kolaborasi yang lebih baik antara kedua tim, yang dapat menghasilkan pengembangan yang lebih cepat dan efisien.

2. DevOps dapat membantu mengotomatisasi proses deployment. Hal ini dapat membantu mengurangi risiko kesalahan dan meningkatkan keandalan aplikasi.

3. DevOps dapat membantu meningkatkan pemantauan dan pemecahan masalah aplikasi. Hal ini dapat membantu mengidentifikasi dan memperbaiki masalah dengan cepat, yang dapat meningkatkan waktu operasional aplikasi.
### DevOps Implementation
**KitaBisa.com** adalah platform crowdfunding Indonesia yang telah menerapkan DevOps untuk meningkatkan pengembangan dan deployment aplikasinya. KitaBisa.com menggunakan berbagai alat dan praktik DevOps, termasuk Docker, Kubernetes, dan Jenkins. Hal ini telah membantu KitaBisa.com memperpendek siklus pengembangan dan meningkatkan kualitas aplikasinya.

**KitaBisa.com** menggunakan Docker untuk membuat kontainer-kontainer aplikasinya. Hal ini memudahkan dalam deployment aplikasi ke lingkungan yang berbeda, seperti development, staging, dan production. Kubernetes digunakan untuk mengelola kontainer-kontainer tersebut dan memastikan bahwa mereka berjalan dengan baik. Jenkins digunakan untuk mengotomatisasi proses deployment.

Manfaat DevOps bagi KitaBisa seperti yang telah dijelaskan sebelumnya, antara lain:

1. Siklus pengembangan yang lebih singkat
2. Meningkatkan kualitas aplikasi
3. Meningkatkan keandalan aplikasi

## 5. Demonstrasi Web Page

Demonstrasi via YouTube : [Here](https://youtu.be/P9BrT40Ni58)
## 6. Publikasi Docker Image
Docker Hub  : [Here](https://hub.docker.com/r/ridwanafazn/invest-threads/tags)
